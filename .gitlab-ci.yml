---
image: ubuntu:latest

# XXX - add a deploy stage to publish man pages, docs, and coverage
# reports

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH

stages:
  - build
  - test

.ubuntu-before_script:
  before_script:
    - export DEBIAN_FRONTEND=noninteractive
    - apt-get update -qq
    - apt-get install --no-install-recommends -y gcc perl liblocale-gettext-perl linux-libc-dev lsb-release make
    - lsb_release -a
    - uname -a

.install-c-build-deps: &install-c-build-deps
  - apt-get install --no-install-recommends -y build-essential apache2-dev autoconf autoconf-archive automake bison dejagnu flex libpam-dev libtool pkg-config python3-all-dev python3-setuptools ruby-dev swig zlib1g-dev

build-all:
  stage: build
  extends:
    - .ubuntu-before_script
  artifacts:
    name: ${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}
    expire_in: 30 days
    untracked: true
    paths:
      - libraries/libapparmor/
      - parser/
      - binutils/
      - utils/
      - changehat/mod_apparmor/
      - changehat/pam_apparmor/
      - profiles/
  script:
    - *install-c-build-deps
    - cd libraries/libapparmor && ./autogen.sh && ./configure --with-perl --with-python --prefix=/usr && make -j $(nproc) && cd ../.. || { cat config.log ; exit 1 ; }
    - make -C parser -j $(nproc)
    - make -C binutils -j $(nproc)
    - make -C utils
    - make -C changehat/mod_apparmor
    - make -C changehat/pam_apparmor
    - make -C profiles

test-libapparmor:
  stage: test
  needs: ["build-all"]
  extends:
    - .ubuntu-before_script
  script:
    - *install-c-build-deps
    # This is to touch the built files in the test stage to avoid needless rebuilding
    - make -C libraries/libapparmor --touch
    - make -C libraries/libapparmor check

test-parser:
  stage: test
  needs: ["build-all"]
  extends:
    - .ubuntu-before_script
  script:
    - *install-c-build-deps
    # This is to touch the built files in the test stage to avoid needless rebuilding
    - make -C parser --touch
    - make -C parser -j $(nproc) tst_binaries
    - make -C parser check

test-binutils:
  stage: test
  needs: ["build-all"]
  extends:
    - .ubuntu-before_script
  script:
    - make -C binutils check

test-utils:
  stage: test
  needs: ["build-all"]
  extends:
    - .ubuntu-before_script
  script:
    # This is to touch the built files in the test stage to avoid needless rebuilding
    - make -C utils --touch

    - apt-get install --no-install-recommends -y libc6-dev libjs-jquery libjs-jquery-throttle-debounce libjs-jquery-isonscreen libjs-jquery-tablesorter flake8 python3-coverage python3-notify2 python3-psutil python3-setuptools python3-tk python3-ttkthemes python3-gi

    # See apparmor/apparmor#221
    - make -C parser/tst gen_dbus
    - make -C parser/tst gen_xtrans
    - make -C utils check
    - make -C utils/test coverage-regression
  artifacts:
    paths:
      - utils/test/htmlcov/
    when: always

test-mod-apparmor:
  stage: test
  needs: ["build-all"]
  extends:
    - .ubuntu-before_script
  script:
    # This is to touch the built files in the test stage to avoid needless rebuilding
    - make -C changehat/mod_apparmor --touch
    - make -C changehat/mod_apparmor check

test-profiles:
  stage: test
  needs: ["build-all"]
  extends:
    - .ubuntu-before_script
  script:
    # This is to touch the built files in the test stage to avoid needless rebuilding
    - make -C profiles --touch
    - make -C profiles check-parser
    - make -C profiles check-abstractions.d
    - make -C profiles check-local

# Build the regression tests (don't run them because that needs kernel access)
test-build-regression:
  stage: test
  needs: ["build-all"]
  extends:
    - .ubuntu-before_script
  script:
    - *install-c-build-deps
      # Additional dependencies required by regression tests
    - apt-get install --no-install-recommends -y attr libdbus-1-dev liburing-dev
    - make -C tests/regression/apparmor -j $(nproc)
shellcheck:
  stage: test
  needs: []
  extends:
    - .ubuntu-before_script
  script:
  - apt-get install --no-install-recommends -y python3-minimal file shellcheck xmlstarlet
  - shellcheck --version
  - './tests/bin/shellcheck-tree --format=checkstyle
       | xmlstarlet tr tests/checkstyle2junit.xslt
       > shellcheck.xml'
  artifacts:
    when: always
    reports:
      junit: shellcheck.xml

# Disabled due to aa-logprof dependency on /sbin/apparmor_parser existing
#   - make -C profiles check-profiles

# test-pam_apparmor:
#  - stage: test
#  - script:
#    - cd changehat/pam_apparmor && make check

include:
  - template: SAST.gitlab-ci.yml
  - template: Secret-Detection.gitlab-ci.yml

variables:
  SAST_EXCLUDED_ANALYZERS: "eslint,flawfinder,semgrep,spotbugs"
  SAST_BANDIT_EXCLUDED_PATHS: "*/tst/*, */test/*"

.send-to-coverity: &send-to-coverity
  - curl https://scan.coverity.com/builds?project=$COVERITY_SCAN_PROJECT_NAME
    --form token=$COVERITY_SCAN_TOKEN --form email=$GITLAB_USER_EMAIL
    --form file=@$(ls apparmor-*-cov-int.tar.gz) --form version="$(git describe --tags)"
    --form description="$(git describe --tags) / $CI_COMMIT_TITLE / $CI_COMMIT_REF_NAME:$CI_PIPELINE_ID"

coverity:
  stage: .post
  extends:
    - .ubuntu-before_script
  only:
    refs:
      - master
    variables:
      - $CI_PROJECT_PATH == "apparmor/apparmor"
  script:
      - apt-get install --no-install-recommends -y curl git texlive-latex-recommended
      - *install-c-build-deps
      - curl -o /tmp/cov-analysis-linux64.tgz https://scan.coverity.com/download/linux64
        --form project=$COVERITY_SCAN_PROJECT_NAME --form token=$COVERITY_SCAN_TOKEN
      - tar xfz /tmp/cov-analysis-linux64.tgz
      - COV_VERSION=$(ls -dt cov-analysis-linux64-* | head -1)
      - PATH=$PATH:$(pwd)/$COV_VERSION/bin
      - make coverity
      - *send-to-coverity
  artifacts:
    paths:
      - "apparmor-*.tar.gz"
